# Dungeon Spawn

*Dynamically create dungeon floorplans for [D&D](http://dnd.wizards.com/) and other role-playing games by rolling dice.*

This is a method to create dungeon layouts for D&D and other role-playing games. 


## Elevator pitch

Rolling on tables is common, but this one provides properties for rooms, tunnels, and other aspects of dungeons to provide the physical layout rather than providing story-based properties.


## Building from code

What you are looking at right now is the source code for the booklet. If you want to download the booklet for use, it's much easier to just download a copy from [http://drivethrurpg](drivethrurpg.com) when it is available.

You only need to be here if you understand XML and [GNU](http://gnu.org) ``make`` and other fancy open source technologies.


### No really, I want to build this.

You can. I've only ever done this on Linux and BSD, so if you're using a non-open source OS, your mileage may vary. 

* [Scribus](http://scribus.net) for ``.sla`` layout files
* [PDFjam](http://www2.warwick.ac.uk/fac/sci/statistics/staff/academic-research/firth/software/pdfjam)

1. Open the ``.sla`` file in Scribus, export as PDF to a directory called ``build`` adjacent to the ``GNUmakefile`` included in this repo.
2. Open a terminal and execute ``make dungeon``
3. The pocketmods get saved to the ``dist`` directory

## License

It's all open source and free culture. 

**Dicey Dungeon** is dual-licensed under the [Open Game
License](http://www.opengamingfoundation.org/ogl.html) and [Creative
Commons BY-SA](http://creativecommons.org/licenses/by-sa/4.0/). You
may use either license, depending on what works best for your project.

Both licenses allow you to modify, distribute, or resell this system.